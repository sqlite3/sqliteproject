/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bug.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (\n"
                    + "                     id,\n"
                    + "                     username,\n"
                    + "                     password\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '3',\n"
                    + "                     'admin1',\n"
                    + "                     '12345'\n"
                    + "                 );";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (\n"
                    + "                     id,\n"
                    + "                     username,\n"
                    + "                     password\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '4',\n"
                    + "                     'admin2',\n"
                    + "                     '12345'\n"
                    + "                 );";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
